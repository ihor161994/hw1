// requires...
const fs = require('fs');
const path = require('path');

// constants...
const folderPath = path.join(__dirname, 'files/');
const extensionsArr = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
const accessFile = {};


function createFile(req, res, next) {
    // Your code to create the file.
    const {filename, content} = req.body;


    fs.readdir('files', (err, data) => {
            if (!filename) {
                res.status(400).json({"message": "Please specify 'filename' parameter"});
            } else if (!content) {
                res.status(400).json({"message": "Please specify 'content' parameter"});
            } else if (!extensionsArr.includes(path.extname(filename))) {
                res.status(400).json({"message": "Invalid extension"});
            } else if (data.includes(filename)) {
                res.status(400).json({"message": `File with '${filename}' already exists`});
            } else if (filename && content) {
                accessFileAdd(req.query, filename);
                fs.writeFileSync(folderPath + filename, content);
                res.status(200).json({"message": "File created successfully"});
            }
        }
    );
}

function getFiles(req, res, next) {

    fs.readdir('files', (err, data) => {
            if (err) {
                res.status(400).json({"message": "Client error"});
            } else {
                res.json({
                    "message": "Success",
                    "files": data
                })
            }
        }
    );
}

const getFile = (req, res, next) => {
    const {filename} = req.params;
    fs.readdir('files', (err, data) => {
            if (data.includes(filename)) {
                const file = data[data.indexOf(filename)]
                const content = fs.readFileSync(folderPath + file, 'utf8');
                if (filename in accessFile) {
                    if (req.query.pass === accessFile[filename]) {
                        res.status(200).send({
                            "message": "Success",
                            "filename": path.basename(file),
                            "content": content,
                            "extension": path.extname(file).split('.').pop(),
                            "uploadedDate": fs.statSync(folderPath + file).birthtime
                        });
                    } else {
                        res.status(400).json({"message": `Invalid password for filename: '${filename}'`});
                    }
                } else {
                    res.status(200).send({
                        "message": "Success",
                        "filename": path.basename(file),
                        "content": content,
                        "extension": path.extname(file).split('.').pop(),
                        "uploadedDate": fs.statSync(folderPath + file).birthtime
                    });
                }
            } else {
                res.status(400).json({"message": `No file with '${filename}' filename found`});
            }
        }
    );
}

// Other functions - editFile, deleteFile
function editFile(req, res, next) {
    const oldFilename = req.params.filename;
    const {filename, content} = req.body;

    fs.readdir('files', (err, data) => {
            if (data.includes(oldFilename)) {
                if (!filename) {
                    res.status(400).json({"message": "Please specify 'filename' parameter"});
                } else if (!content) {
                    res.status(400).json({"message": "Please specify 'content' parameter"});
                } else if (!extensionsArr.includes(path.extname(filename))) {
                    res.status(400).json({"message": "Invalid extension"});
                } else if (data.includes(filename) && filename !== oldFilename) {
                    res.status(400).json({"message": `File with '${filename}' already exists`});
                } else if (filename && content) {
                    if (filename in accessFile) {
                        if (req.query.pass === accessFile[filename]) {
                            fs.writeFileSync(folderPath + oldFilename, content);
                            fs.renameSync(folderPath + oldFilename, folderPath + filename);
                            res.status(200).json({"message": "File update successfully"});
                        } else {
                            res.status(400).json({"message": `Invalid password for filename: '${filename}'`});
                        }
                    } else {
                        fs.writeFileSync(folderPath + oldFilename, content);
                        fs.renameSync(folderPath + oldFilename, folderPath + filename);
                        res.status(200).json({"message": "File update successfully"});
                    }
                }
            } else {
                res.status(400).json({"message": `No file with '${filename}' filename found`});
            }
        }
    );
}

function deleteFile(req, res, next) {
    const {filename} = req.params;

    fs.readdir('files', (err, data) => {
            if (data.includes(filename)) {
                const file = data[data.indexOf(filename)]
                if (filename in accessFile) {
                    if (req.query.pass === accessFile[filename]) {
                        delete accessFile[filename];
                        fs.unlinkSync(folderPath + file);
                        res.status(200).json({"message": "File deleted successfully"});
                    } else {
                        res.status(400).json({"message": `Invalid password for filename: '${filename}'`});
                    }
                } else {
                    fs.unlinkSync(folderPath + file)
                    res.status(200).json({"message": "File deleted successfully"});
                }
            } else {
                res.status(400).json({"message": `No file with '${filename}' filename found`});
            }
        }
    );
}


function accessFileAdd(obj, filename) {
    if ('pass' in obj) {
        accessFile[filename] = obj.pass;
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}


